osmnx (1.2.3+ds-1) unstable; urgency=medium

  * New upstream nano version (Closes: #1025588).
  * Debianization:
    - d/copyright:
      - Files-Excluded list, adjust.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 17 Dec 2022 13:43:17 +0000

osmnx (1.2.2+ds-2) unstable; urgency=medium

  * Debianization:
    - d/control:
      - Depends fields, drop python3-descarted dependencies (Closes: #1025044);
        thanks to Bas Couwenberg <sebastic@xs4all.nl> for noticing the issue;
      - Built-Using, python-osmnx-doc Package, discard.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 03 Dec 2022 11:02:14 +0000

osmnx (1.2.2+ds-1) unstable; urgency=medium

  * New upstream nano version.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 14 Aug 2022 16:49:34 +0000

osmnx (1.2.1+ds-2) unstable; urgency=medium

  * Debianization:
    - d/t/control:
      - online test, add needs-internet and flaky Restrictions
       (Closes: #1013371).

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 25 Jun 2022 15:51:14 +0000

osmnx (1.2.1+ds-1) unstable; urgency=medium

  * New upstream nano version.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 19 Jun 2022 15:05:47 +0000

osmnx (1.2.0+ds-1) unstable; urgency=medium

  * New upstream minor version.
  * Debianization:
    - d/copyright:
      - copyright year-tuples, update;
    - d/control:
      - Standards-Version, bump to 4.6.1 (no change).

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 28 May 2022 15:40:21 +0000

osmnx (1.1.2+ds-1) unstable; urgency=medium

  * New upstream nano version.
  * Debianization:
    - d/watch, update;
    - d/control:
      - doc package, add Multi-Arch metadata;
    - d/rules:
      - override_dh_installexamples target, discard coding material;
      - override_dh_fixperms target, obsoleted.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 19 Nov 2021 10:50:29 +0000

osmnx (1.1.1+ds-5) unstable; urgency=medium

  * Debianization:
     - d/patches/*:
       - d/p/{adhoc-fix-elevation-multiprocessing,
           debianization-tests-extra}.patch, introduce.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 04 Nov 2021 15:47:34 +0000

osmnx (1.1.1+ds-4) unstable; urgency=medium

  * Debianization:
    - d/tests: enhance.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 23 Oct 2021 18:12:03 +0000

osmnx (1.1.1+ds-3) unstable; urgency=medium

  * Debianization:
    - d/control:
      - Standards-Version, bump to 4.6.0 (no change);
    - d/rules:
      - override_dh_auto_clean target, introduce to harden debclean;
    - d/tests/control:
      - online tests:
        - Restrictions field, add needs-internet (Closes: #995021).

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 11 Oct 2021 14:28:43 +0000

osmnx (1.1.1+ds-2) unstable; urgency=medium

  * Upload to unstable.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 29 Aug 2021 16:04:01 +0000

osmnx (1.1.1+ds-1) experimental; urgency=medium

  * New minoe version.
  * Debianization:
    - d/copyright:
      - Files-Excluded list, refresh;
    - d/control:
      - Depends and Recommends fiels, update;
    - d/tests/control:
      - Depends field, upstream test, update;
    - d/rules:
      - override_dh_prep target, remove executable bit to t/est_osmnx.py .

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 20 Jun 2021 13:57:14 +0000

osmnx (1.0.1+ds-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Jerome Benoit ]
  * new upstream major version.
  * d/copyright:
    - Files-Excluded list, refresh;
    - copyright year tuples, update;
  * d/control:
    - Build-Depends field, update.
  * d/p/debianization.patch, update;
  * d/rules, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 22 Jan 2021 14:50:39 +0000

osmnx (0.16.2+ds-2) unstable; urgency=medium

  * d/t/test_osmnx_offline.py is now really an offline test.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 30 Nov 2020 16:19:44 +0000

osmnx (0.16.2+ds-1) unstable; urgency=medium

  * New upstream version.
  * d/p/debianization-tests-neutralize-folium.patch, no more needed.
  * d/t/test_osmnx_offline.py, update.
  * d/control, bump Standards-Version to 4.5.1 (no change).
  * d/control, update Recommends field.
  * d/tests/control, update Depends fields.

 -- Jerome Benoit <calculus@rezozer.net>  Mon, 30 Nov 2020 14:12:00 +0000

osmnx (0.14.1+ds-2) unstable; urgency=medium

  [ Jerome Benoit]
  * d/control: bump dh to 13.
  * d/u/metadata: introduce.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Jerome Benoit <calculus@rezozer.net>  Thu, 05 Nov 2020 21:08:01 +0000

osmnx (0.14.1+ds-1) unstable; urgency=medium

  * New upstream release.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 13 Jun 2020 18:42:55 +0000

osmnx (0.14.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/control:
      - Files-Excluded field, update;
    - debian/patches/*, refresh;
    - debian/tests/*:
      - d/t/control, update;
      - t/d/test_osmnx_offline.py, refresh.

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 06 Jun 2020 16:26:07 +0000

osmnx (0.12.1+ds-1) unstable; urgency=medium

  * New upstream release.
  * Debianization:
    - debian/control:
      - Build-Depends list, harden.

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 05 May 2020 07:43:06 +0000

osmnx (0.11.4+ds-1) unstable; urgency=medium

  * New upstream version.
  * Debianization:
    - debian/copyright:
      - copyright year tuple, update;
      - Files-Excluded field, refresh;
    - debian/watch, fix;
    - debian/control:
      - Standards-Version, bump to 4.5.0.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 02 Feb 2020 18:13:17 +0000

osmnx (0.11+ds-2) unstable; urgency=medium

  * Fix Release Critical bug due texliv-base transition (Closes: #947726).
  * Debiaization:
    - debian/control:
      - Build-Depends-Indep field, replace texlive-generic-extra by
        texlive-plain-generic.

 -- Jerome Benoit <calculus@rezozer.net>  Sun, 29 Dec 2019 17:57:41 +0000

osmnx (0.11+ds-1) unstable; urgency=medium

  * New upstream version.
  * Debianization:
    - debian/copyright:
      - Files-Excluded field, update;
    - debian/control:
      - Recommends field, add python3-tk;
    - debian/patches/*:
      - d/p/debianization{,-tests-neutralize-folium}.patch , refresh;
      - d/p/debianization-tests-neutralize-stats_stats4.patch, obsoleted.

 -- Jerome Benoit <calculus@rezozer.net>  Fri, 27 Dec 2019 14:32:31 +0000

osmnx (0.10+ds-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Jerome Benoit ]
  * Set Rules-Requires-Root to no
  * Add to Build-Depends python3-pytest.
  * Add to {Build-,}Depends python3-distutils.
  * Introduce d/p/debianization-tests-neutralize-stats_stats4.patch .

 -- Jerome Benoit <calculus@rezozer.net>  Sat, 16 Nov 2019 08:47:00 +0000

osmnx (0.10+ds-1) unstable; urgency=medium

  * Initial release. (Closes: #852200)

 -- Jerome Benoit <calculus@rezozer.net>  Tue, 23 Jul 2019 17:14:11 +0000
